<?php

class page_model extends CI_Model
{

		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
        }
        
		public function get_precios($data)
		{
			$this->db->select('*');
			$this->db->where("puerto_id",6);
			$this->db->from('precios');
			$query = $this->db->get();

			if ($query->num_rows() >= 1) {
				return $query->result();
			} else {
				return false;
			}
		}

        public function get_cooperativas()
        {
            $db2 = $this->load->database('landing', TRUE);
            $result = $db2->get('cooperativas');
            return $result->result();
        }
		

        public function get_rubros()
        {   

            $db2 = $this->load->database('landing', TRUE);

            $db2->select('*');
            $result = $db2->get('rubros');

            return $result->result();
        }


        public function get_subgrupos($id_rubro)
        {   

            $db2 = $this->load->database('landing', TRUE);

            $db2->where('id_rubro', $id_rubro);
            $query = $db2->get('subrubros');
            return $query->result();

        }


        public function get_subgrupos_id($id_subrubros)
        {   

            $db2 = $this->load->database('landing', TRUE);
            $db2->select('marcas.*');
            $db2->where('subrubros_marcas.id_subrubros', $id_subrubros);
            $db2->join('marcas', 'marcas.id = subrubros_marcas.id_marca');
            $query = $db2->get('subrubros_marcas');
            return $query->result();

        }
		
		
		public function get_dolar()
		{
			$this->db->select('*');
			$this->db->from('dolar');
			$query = $this->db->get();

			if ($query->num_rows() >= 1) {
				return $query->result();
			} else {
				return false;
			}			
		}
		
        
        public $title;
        public $content;
        public $date;


        public function get_count() {

            $this->db->select("count(*) as count, productos.*");

            $this->db->group_by('id_vigencia');
            $this->db->where('id_vigencia' , 2);

            $query = $this->db->get('productos');
            return $query->result();
        }
        
        
        public function save_product_click()
        {
        
        	// --        
        	// Save post
        	// --
        	if(isset($_POST["id_producto"]) && isset($_POST["id_session"]))
        	{
        		$productos = $this->get_producto_id($_POST["id_producto"]);
        		$producto = $productos[0];

	        	$this->db->set("visit_id",$_POST["id_session"]);
        		$this->db->set("producto_id",$producto->id);
        		$this->db->set("titulo",$producto->titulo);
    	    	$this->db->set("id_promocion",$producto->id_promocion);
        		$this->db->set("id_marca",$producto->id_marca);
	        	$this->db->set("id_vigencia",$producto->id_vigencia);
        		$this->db->set("descuento",$producto->descuento);
    	    	$this->db->set("descuento_semanal",$producto->descuento_semanal);
	        	$this->db->set("precio",$producto->precio);
        		$this->db->set("imagen_producto",$producto->imagen_producto);
        		$this->db->set("liquidacion",$producto->liquidacion);
    	    	$this->db->set("precio_oferta",$producto->precio_oferta);
	        	$this->db->set("promocion_txt",$producto->promocion_txt);
        		$this->db->set("descripcion",$producto->descripcion);
        		$this->db->set("tamanio_column",$producto->tamanio_column);
    	    	$this->db->set("fecha_inicio",$producto->fecha_inicio);
	        	$this->db->set("fecha_fin",$producto->fecha_fin);
        			
        		$this->db->set("added_at",time());
			    $this->db->set("modified_at",time());
	        	$this->db->insert("visits_clicks_productos");
        	}
        }
        
        // ---
        // save visit into database
        // ---
        public function save_visit($productos)
        {
        
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			    $ip = $_SERVER['REMOTE_ADDR'];
			}

        	$this->db->set("user_id",$this->session->userdata["logged_in"]["id"]);
        	$this->db->set("email",$this->session->userdata["logged_in"]["mail"]);
        	$this->db->set("__ci_last_regenerate",$this->session->userdata["__ci_last_regenerate"]);
        	$this->db->set("ip",$ip);
        	$this->db->set("user_agent",$_SERVER['HTTP_USER_AGENT']);
        	$this->db->set("added_at",time());
        	$this->db->set("modified_at",time());
        	$this->db->insert("visits");
        	$visit_id = $this->db->insert_id();
        	
        	// ----        	
        	// Saving visits details
        	// ----
        	foreach($_GET as $get => $k)
        	{
        		foreach($k as $kk)
        		{
        			$this->db->set("visit_id",$visit_id);
        			$this->db->set("tipo","filtro");
        			$this->db->set("variable",$get);
        			$this->db->set("valor",$kk);
        			$this->db->set("added_at",time());
		        	$this->db->set("modified_at",time());
        			$this->db->insert("visits_details");
        		}
        	}

        	// --        	
        	// -- saving productos
        	// --
        	foreach($productos as $producto)
        	{
        		$this->db->set("visit_id",$visit_id);
        		$this->db->set("producto_id",$producto->id);
        		$this->db->set("titulo",$producto->titulo);
        		$this->db->set("id_promocion",$producto->id_promocion);
        		$this->db->set("id_marca",$producto->id_marca);
        		$this->db->set("id_vigencia",$producto->id_vigencia);
        		$this->db->set("descuento",$producto->descuento);
        		$this->db->set("descuento_semanal",$producto->descuento_semanal);
        		$this->db->set("precio",$producto->precio);
        		$this->db->set("imagen_producto",$producto->imagen_producto);
        		$this->db->set("liquidacion",$producto->liquidacion);
        		$this->db->set("precio_oferta",$producto->precio_oferta);
        		$this->db->set("promocion_txt",$producto->promocion_txt);
        		$this->db->set("descripcion",$producto->descripcion);
        	    $this->db->set("tamanio_column",$producto->tamanio_column);
        	    $this->db->set("fecha_inicio",$producto->fecha_inicio);
        		$this->db->set("fecha_fin",$producto->fecha_fin);
        		
        		$this->db->set("added_at",time());
		        $this->db->set("modified_at",time());
        		$this->db->insert("visits_productos");
        	}
        	
        	return $visit_id;
        }

        public function insert_sorteo($data)
        {	
        	$config = Array(
    	        'protocol' => 'smtp',
    	        'smtp_host' => 'ssl://smtp.googlemail.com',
    	        'smtp_port' => 465,
    	        'smtp_user' => 'diego.mantovani@gmail.com',
    	        'smtp_pass' => 'p4t0f30p4t0f30',
    	        'mailtype'  => 'html', 
    	        'charset'   => 'utf-8'
    	    );
    	    $this->load->library('email', $config);
    	    $this->email->set_newline("\r\n");


    	    $asunto = '¡Sorteo dia del amigo!';

        	$mensaje = '<p style="font-family:"Arial", sans-serif;margin:0">Hola!<br><br>

			¡Gracias por participar! Mucha suerte, el lunes 22 de julio comunicaremos los ganadores.</p>
			<img src="http://bertoldiclub.com/asset/img/logomails.png" width="130" style="margin-top:40px;display:block;">';		    

    	    $name = 'Berdoldi Club';
    	    $email = $this->session->userdata['logged_in']['mail'];

    	    $this->email->from($email,$name);
    	    $this->email->to($this->session->userdata['logged_in']['mail']);
    	    //$this->email->cc('tmk2@bertoldi.com.ar');
    		//$this->email->cc('diego.mantovani@gmail.com');
    	    $this->email->subject($asunto);
    	    $this->email->message($mensaje);
    	    if ( ! $this->email->send()) {
		        show_error($this->email->print_debugger());
		    } else {

        		$this->db->insert('sorteo', $data);
        	}
        }

		public function insert_registro($data)
        {	
    		$config = Array(
    	        'protocol' => 'smtp',
    	        'smtp_host' => 'ssl://smtp.googlemail.com',
    	        'smtp_port' => 465,
    	        'smtp_user' => 'diego.mantovani@gmail.com',
    	        'smtp_pass' => 'p4t0f30p4t0f30',
    	        'mailtype'  => 'html', 
    	        'charset'   => 'utf-8'
    	    );
    	    $this->load->library('email', $config);
    	    $this->email->set_newline("\r\n");


    	    $asunto = $_POST['nombre'].' ¡ya sos parte de Bertoldi Club!';

        	$mensaje = '


        	<body style="background: #ccc;padding:40px;text-align:center;">
        	<style type="text/css">
        		.link-mes {text-decoration: none;background: #c63975;color: #fff;padding: 8px 25px;display: inline-block;font-size: 18px;font-family:"Arial", sans-serif;margin-bottom: 40px;margin-top:40px;}
        		.parrafo-desc {font-family:"Arial", sans-serif;font-size: 19px;padding: 30px 50px;margin:0;}
        	</style>
	        	<div style="background:#fff;width:650px;margin:0 auto;text-align:center;">
	        		<img src="https://www.oxford.com.ar/mailings/bertoldi/curso/curso_header.png" style="display:block;width:100%;height:auto;" width="650">
					<p class="parrafo-desc" style="font-family:Arial, sans-serif;font-size: 19px;padding: 30px 50px;margin:0;">Hola, '.$_POST['nombre'].'<br><br>
					¡Gracias por registrarte! Ya podés ingresar al Club y conocer todos los beneficios que tenemos para vos.</p>
					<a class="link-mes" href="http://bertoldiclub.com/home/inicio/" style="text-decoration: none;background: #c63975;color: #fff;padding: 8px 25px;display: inline-block;font-size: 18px;font-family:Arial, sans-serif;margin-bottom: 40px;margin-top:40px;" target="_blank">Ingresar</a>
		        </div>
        	</body>';		    

    	    $name = $_POST['nombre'];
    	    $email = $_POST['mail'];

    	    $this->email->from($email,$name);
    	    $this->email->to($_POST['mail']);
    	    //$this->email->cc('tmk2@bertoldi.com.ar');
    		//$this->email->cc('diego.mantovani@gmail.com');
    	    $this->email->subject($asunto);
    	    $this->email->message($mensaje);
    	    if ( ! $this->email->send()) {
		        show_error($this->email->print_debugger());
		    } else {
		    	$this->db->insert('lead', $data);
		    }
                


        }

        public function insert_lead($data)
        {
            $db2 = $this->load->database('landing', TRUE);
            $db2->insert('presupuestos', $data);
        }
        public function insert_lead_usado($data)
        {
            $this->db->insert('lead_usados', $data);
        }

        public function login($data) {

			$condition = "lead.mail =" . "'" . $data['mail'] . "' AND " . "lead.password =" . "'" . $data['password'] . "'";
			$this->db->select('lead.*');
			$this->db->from('lead');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}

		public function read_user_information($username) {
			$condition = "mail =" . "'" . $username . "'";
			$this->db->select('lead.*');
			$this->db->from('lead');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return $query->result();
			} else {
				return false;
			}
		}
		
		public function get_active_provinces()
        {
			$this->db->select('provincia.*');
			$this->db->join('curso_provincia', 'curso_provincia.provinciaId = provincia.id');
			$this->db->group_by('provincia.id');
			$query = $this->db->get('provincia');
			return $query->result();				
        }
		public function get_province($id)
        {
			if($id<>0){
				$this->db->where('id',$id);
				$query = $this->db->get('provincia');
				return $query->result();
			}							
        }

        public function get_promociones()
        {
                $query = $this->db->get('promociones');
                return $query->result();
        }

        public function get_marcas()
        {       
                $db2 = $this->load->database('landing', TRUE);
                $db2->order_by('orden', 'asc');
                $db2->from('marcas');
                $query = $db2->get();
                return $query->result();
        }

        public function get_vigencias()
        {       $this->db->order_by('nombre','asc');
                $query = $this->db->get('vigencias');
                return $query->result();
        }

        public function get_productos_promos_semanales()
        {
            $this->db->select('productos.*');
            
            //$this->db->select('categorias.nombre as categoria');
            $this->db->select('promociones.nombre as promocion');
            $this->db->select('promociones.imagen_promocion as imagen_promocion');
            $this->db->select('promociones.slug as slug_promocion');
            $this->db->select('marcas.imagen as imagen_marca');


            $this->db->join('marcas', 'marcas.id = productos.id_marca');
            $this->db->join('promociones','promociones.id = productos.id_promocion');
            $this->db->where('productos.descuento_semanal', 'si');
            $query = $this->db->get('productos');
            return $query->result();
        }
		
		
		public function get_producto_id($id)
		{
			$this->db->select("*");
            $this->db->where('id', $id);
            $query = $this->db->get('productos');
            return $query->result();
		}
		
        public function get_productos($filter,$limit, $start)
        {		
                $this->db->limit($limit, $start);
        		$this->db->select('productos.*');
        		//$this->db->select('categorias.nombre as categoria');
        		$this->db->select('promociones.nombre as promocion');
        		$this->db->select('promociones.imagen_promocion as imagen_promocion');
                $this->db->select('promociones.slug as slug_promocion');

        		$this->db->join('promociones','promociones.id = productos.id_promocion');

                $this->db->where('productos.descuento_semanal', 'no');
                $this->db->where('productos.id_vigencia', 2);

                

                //$query = $this->db->get('productos');
                // print_r($query->result());
                // ---          
                // Armo el filtro.
                // ---
                if(count($_GET) > 0)
                {

                    foreach($_GET as $get => $k)
                    {
                        switch ($get) {
                            case "categoria":
                                if($i_categoria <= 0)
                                {

                                    $this->db->join("filtros_productos","filtros_productos.id_producto = productos.id");
                                    $this->db->join("categorias","categorias.id = filtros_productos.id_categoria");
                                    
                                }
                            $this->db->group_start();
                                foreach($k as $kk)
                                {       
                                    //print_r($kk);
                                    //exit;

                                    $this->db->or_like("filtros_productos.id_categoria",$kk);
                                    $this->db->group_by("filtros_productos.id_producto");
                                    echo '<style type="text/css">.cat_'.$kk.' {opacity: .2;pointer-events: none;}</style>';

                                }
                            $this->db->group_end();
                                $i_categoria++;

                            break;
                            

                            case "promos":
                                if($i_promo <= 0)
                                {
                                    
                                }
                            $this->db->group_start();
                                foreach($k as $kk)
                                {                           
                                    $this->db->or_like("promociones.id",$kk);
                                    echo '<style type="text/css">.prom {opacity: .2;pointer-events: none;}</style>';
                                }
                            $this->db->group_end();
                                $i_promo++;
                            break;

                            case "marcas":
                                if($i_marcas <= 0)
                                {
                                    
                                    $this->db->join("marcas","marcas.id = productos.id_marca");
                                }
                            $this->db->group_start();
                                foreach($k as $kk)
                                {                           
                                    $this->db->or_like("marcas.id",$kk);
                                }
                            $this->db->group_end();
                                $i_marcas++;
                            break;

                            case "vigencias":
                                if($i_vigencia <= 0)
                                {
                                    
                                    $this->db->join("vigencias","vigencias.id = productos.id_vigencia");
                                }
                            $this->db->group_start();
                                foreach($k as $kk)
                                {                           
                                    $this->db->or_like("vigencias.id",$kk);
                                    echo '<style type="text/css">.vig_'.$kk.' {opacity: .2;pointer-events: none;}</style>';
                                }
                            $this->db->group_end();
                                $i_vigencia++;
                            break;
                        }

                    }

                }
                $query = $this->db->get('productos');
               // print_r($query->result());
                return $query->result();
        }

        public function build_filter_list()
        {
            $filtros = array();
            
            if(count($_GET) > 0)
            {
                $i=0;
                foreach($_GET as $get => $k)
                {
                    switch ($get) {
                        case "categoria":
                            foreach($k as $kk)
                            {
                                $filtros[$i]["id"] = $kk;
                                $fab = $this->get_categoria_id($kk);
                                $filtros[$i]["nombre"] = $fab[0]->nombre;
                                $filtros[$i]["type"] = $get;
                                $i++;
                            }
                        break;

                        case "promos":
                            foreach($k as $kk)
                            {
                                $filtros[$i]["id"] = $kk;
                                $fab = $this->get_promociones_id($kk);
                                $filtros[$i]["nombre"] = $fab[0]->nombre;
                                $filtros[$i]["type"] = $get;
                                $i++;
                            }
                        break;

                        case "marcas":
                            foreach($k as $kk)
                            {
                                $filtros[$i]["id"] = $kk;
                                $fab = $this->get_marcas_id($kk);
                                $filtros[$i]["nombre"] = $fab[0]->nombre;
                                $filtros[$i]["type"] = $get;
                                $i++;
                            }
                        break;

                        case "vigencias":
                            foreach($k as $kk)
                            {
                                $filtros[$i]["id"] = $kk;
                                $fab = $this->get_vigencias_id($kk);
                                $filtros[$i]["nombre"] = $fab[0]->nombre;
                                $filtros[$i]["type"] = $get;
                                $i++;
                            }
                        break;
                    }
                }
            }
            
            return $filtros;
        }

        // --
        // build_remove_link
        // --
        public function build_remove_link($filtro,$where)
        {
            //print_r($where);
            //exit;
            $string_get="";
            $i=0;
            foreach($_GET as $get => $k)
            {
                if($i<=0) $symbol="?";
                else      $symbol="&";
                
                // --
                // si no es el que deleteo
                // --
                if(is_array($k))
                {
                    foreach($k as $kk)
                    {
                        if($filtro.$where !== $get.$kk)
                        {
                            if($i<=0) $symbol="?";
                            else      $symbol="&";
                
                            $string_get.=$symbol.$get."[]=".$kk;
                            $i++;
                        }
                    }
                }
                else
                {
                    if(strcmp($filtro.$where, $get.$k) > 0)
                    {
                        $string_get.=$symbol.$get."[]=".$k;
                        $i++;
                    }               
                }
            }
            
            //print_r($string_get);
            //exit;
            return $string_get;
        }

        // ---      
        // GET fabricante by id
        // ---

        public function get_categoria_id($id)
        {
            $this->db->select("categorias.*");
                    
            $this->db->where('categorias.id', $id);
            $query = $this->db->get('categorias');
            return $query->result();    
        }

        public function get_promociones_id($id)
        {
            $this->db->select("promociones.*");
                    
            $this->db->where('promociones.id', $id);
            $query = $this->db->get('promociones');
            return $query->result();    
        }

        public function get_marcas_id($id)
        {
            $this->db->select("marcas.*");
                    
            $this->db->where('marcas.id', $id);
            $query = $this->db->get('marcas');
            return $query->result();    
        }

        public function get_vigencias_id($id)
        {
            $this->db->select("vigencias.*");
                    
            $this->db->where('vigencias.id', $id);
            $query = $this->db->get('vigencias');
            return $query->result();    
        }



        public function get_class_filter($producto_id)
        {
            $this->db->where("filtros_productos.id_producto",$producto_id);

            $this->db->select("categorias.slug as slug_class");

            $this->db->join("categorias","categorias.id = filtros_productos.id_categoria","left");

            $this->db->group_by("categorias.slug");

            $query = $this->db->get('filtros_productos');

            $resultados = $query->result();

            

            $clases= "";
            foreach($resultados as $r)
            {
                //print_r($r);
                //exit;
                $clases.=$r->slug_class." ";
            }
            return $clases;
        }

        public function get_actual_gets()
        {
            $string_get="";
            $i=0;
            foreach($_GET as $get => $k)
            {
                if($i<=0) $symbol="?";
                else      $symbol="&";
                
                if(is_array($k))
                {
                    foreach($k as $kk)
                    {
                        if($i<=0) $symbol="?";
                        else      $symbol="&";
                        
                        $string_get.=$symbol.$get."[]=".$kk;
                        $i++;
                    }
                    $i++;
                }
                else
                {
                    $string_get.=$symbol.$get."[]=".$k;     
                    $i++;       
                }

            }
            return $string_get;
        }


        public function agrega_filtro($filtro,$where)
        {
            $actual_get=$this->get_actual_gets();
            
            
            $pos = strpos($actual_get, "?");
            if ($pos === false) {
                $actual_get.="?".$filtro."[]=".$where;
            }
            else
            {
                $actual_get.="&".$filtro."[]=".$where;          
            }
            
            
            
            return $actual_get;
        }

        public function get_categorias_structure($id_parent, $html)
        {       
                $this->db->where('parent_id', $id_parent);
                $this->db->order_by('nombre','asc');
                $query = $this->db->get('categorias');
                if($id_parent != 0):
                    $class_secundaria = 'secundario';
                else:
                    $class_secundaria = '';
                endif;
                $html.='';
                foreach($query->result() as $key => $fila):
                        if($key > 4){
                            $html.='<a href="'.base_url().'club/'.$this->agrega_filtro("categoria",$fila->{'id'}).'" class="cat_'.$fila->{'id'}.' hidden">'.$fila->{'nombre'}.'</a>';
                        }else {
                            $html.='<a href="'.base_url().'club/'.$this->agrega_filtro("categoria",$fila->{'id'}).'" class="cat_'.$fila->{'id'}.'">'.$fila->{'nombre'}.'</a>';
                        }
                        
                        $html.= $this->get_categorias_structure($fila->{'id'}, '');
                endforeach;
                $html.='';
                return $html;
        }

        public function get_leads()
        {
			$query = $this->db->get('lead');
			return $query->result();
										
        }


        public function get_usadas()
        {
            $query = $this->db->get('usadas');
            return $query->result();
                                        
        }

        public function get_user_info($id_user)
        {   
            $this->db->where('id',$id_user);
            $query = $this->db->get('lead');
            return $query->result();
                                        
        }

		
}

?>