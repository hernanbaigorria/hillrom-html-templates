<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
      // $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   // Load session library
	   $this->load->library('session');

	} 
	 
	
	public function index()
	{
		
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');
		
		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
	   
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título

		//$image = base_url().'asset/';
		$this->template->write('title', 'Hillrom - Template', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', $image);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();

		// ---		
		// Obtengo los precios.
		// ---

		$data = '';
		
		
		$this->template->write_view('content', 'layout/home/home', $data);
		$this->template->write_view('header', 'layout/header', $data);
		$this->template->write_view('footer', 'layout/footer');   
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}


	public function template_01()
	{
		
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');
		
		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
	   
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título

		//$image = base_url().'asset/';
		$this->template->write('title', 'Hillrom - Template', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', $image);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();

		// ---		
		// Obtengo los precios.
		// ---

		$data = '';
		
		
		$this->template->write_view('content', 'layout/home/template_01', $data);
		$this->template->write_view('header', 'layout/header', $data);
		$this->template->write_view('footer', 'layout/footer');   
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function template_02()
	{
		
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');
		
		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
	   
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título

		//$image = base_url().'asset/';
		$this->template->write('title', 'Hillrom - Template', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', $image);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();

		// ---		
		// Obtengo los precios.
		// ---

		$data = '';
		
		
		$this->template->write_view('content', 'layout/home/template_02', $data);
		$this->template->write_view('header', 'layout/header', $data);
		$this->template->write_view('footer', 'layout/footer');   
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

		public function template_03()
		{
			
			// ----------------------------
			// testing templating method
			// ----------------------------
		
			//como hemos creado el grupo registro podemos utilizarlo
		    $this->template->set_template('template');

			$this->template->add_css('asset/css/home.css');
			
			// --		
			// Save utm
			// --
		    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
		    {
		    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
		    }
		   
		    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
		    {
	 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
	 	    }
		   
			
			//añadimos los archivos js que necesitemoa		
			//$this->template->add_js('asset/js/home.js');
		    
			//desde aquí también podemos setear el título

			//$image = base_url().'asset/';
			$this->template->write('title', 'Hillrom - Template', TRUE);
			$this->template->write('description', '', TRUE);
			$this->template->write('keywords', '', TRUE);
			$this->template->write('image', '', $image);
			$this->template->write('ogType', 'website', TRUE);
			//obtenemos los usuarios
			//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
			$CI =& get_instance();

			// ---		
			// Obtengo los precios.
			// ---

			$data = '';
			
			
			$this->template->write_view('content', 'layout/home/template_03', $data);
			$this->template->write_view('header', 'layout/header', $data);
			$this->template->write_view('footer', 'layout/footer');   
			
			//con el método render podemos renderizar y hacer que se visualice la template
		    $this->template->render();
		
			 //$this->load->view('welcome_message');
		}


	public function generate_code()
	{

		header('Content-disposition: attachment; filename=template.html');
	   header('Content-type: application/txt');
	   echo $_POST['myTextarea'];
	   exit; //stop writing
	}

	public function test()
	{
		
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');
		
		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
	   
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título

		//$image = base_url().'asset/';
		$this->template->write('title', 'Hillrom - Template', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', $image);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();

		// ---		
		// Obtengo los precios.
		// ---
		$data["precios"] = $this->page_model->get_precios('');
		$data["dolar"] = $this->page_model->get_dolar();

		$data['marcas'] = $this->page_model->get_marcas();
		$data['rubros'] = $this->page_model->get_rubros();
		
		
		$this->template->write_view('content', 'layout/home/home_test', $data);
		$this->template->write_view('header', 'layout/header', $data);
		$this->template->write_view('footer', 'layout/footer');   
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function get_rubros()
	{

		$subgrupos = $this->page_model->get_subgrupos($_POST['idrubro']);

		$html.= '<select id="subrubro"><option value="">Seleccionar subrubro...</option>';

		foreach($subgrupos as $sub):
			$html.='<option value="'.$sub->id.'">'.$sub->nombre.'</option>';
		endforeach;

		$html.= '</select>';

		$script = '<script>
		$(document).ready(function(){
			$("#subrubro").change(function(){
		    	var idsubrubro = $(this).val();

  		    $.ajax({
  		        url: "'.base_url().'home/get_marcas/",
  		        type: "post",
  		        data: {idsubrubro:idsubrubro},
  		        success:function(response){
  		            $("#imagesmarcas").show();
  		            $("#imagesmarcas").html(response);
  		        }
  		    });
			});
		});</script>
		';

		echo json_encode(array(
        'html' =>  $html,
        'script' => $script
	   	));

	}


	public function get_marcas()
	{

		$marcas = $this->page_model->get_subgrupos_id($_POST['idsubrubro']);
		$html = '';
		foreach($marcas as $marca):
			$html.='
			<div class="col-12 col-md-2 text-center">
				<a href="'.$marca->link.'" target="_blank" class="imagen-logo" style="background:url('.base_url().'asset/img/uploads/'.$marca->{'imagen'}.');"></a>
			</div>
			';
		endforeach;

		echo $html;

	}

	public function save_contact()
	{

		$utm_medium = $this->session->userdata("utm_medium");
		$utm_source = $this->session->userdata("utm_source");
		$data = array(
			'nombre' => $_POST['nombre'],
			'telefono' => $_POST['telefono'],
			'email' => $_POST['email'],
			'mensaje' => $_POST['mensaje'],
			'cliente' => $_POST['tipo'],
			'added_at' => date("Y-m-d H:i:s"),
			'utm_source' => $utm_source,
			'utm_medium' => $utm_medium,
			'modified_at' => date("Y-m-d H:i:s"),
			'estado_id' => 1,
		);

		$this->page_model->insert_lead($data);

		$config = Array(
	        'protocol' => 'smtp',
	        'smtp_host' => 'ssl://smtp.googlemail.com',
	        'smtp_port' => 465,
	        'smtp_user' => 'diego.mantovani@gmail.com',
	        'smtp_pass' => 'P4t0f30p4t0f30!',
	        'mailtype'  => 'html', 
	        'charset'   => 'utf-8'
	    );

	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");

	    $asunto = 'Hillrom - Template - Nueva consulta de '.$_POST['nombre'].'';

    	$mensaje = '<h1>Datos ingresados</h1>
	    			<b>Nombre: </b>'.$_POST['nombre'].'<br>
	    			<b>Telefono: </b>'.$_POST['telefono'].'<br>
	    			<b>Email: </b>'.$_POST['email'].'<br>
	    			<b>Mensaje: </b>'.$_POST['mensaje'].'';		    

	    $name = $_POST['nombre'];
	    $email = $_POST['email'];

	    $this->email->from($email,$name);
	    $this->email->to('info@Hillrom - Template.com.ar');
	    $this->email->cc('diego.mantovani@gmail.com');
	    $this->email->subject($asunto);
	    $this->email->message($mensaje);

	    if ( ! $this->email->send()) {
	        show_error($this->email->print_debugger());
	    } else {
	    	if($_POST['tipo'] == 'Empresa'){
	    		echo "empresa";
	    		exit;
	    	} else {
	    		echo "productor";
	    		exit;
	    	}
	    }

	}

	public function gracias_empresa()
	{
		
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');
		
	   
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título

		//$image = base_url().'asset/';
		$this->template->write('title', 'Hillrom - Template - Gracias', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', $image);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		
		
	
		$this->template->write_view('content', 'layout/home/gracias', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function gracias_productor()
	{
		
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');
		
	   
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título

		//$image = base_url().'asset/';
		$this->template->write('title', 'Hillrom - Template - Gracias', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', $image);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		
		
	
		$this->template->write_view('content', 'layout/home/gracias', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function mailing01()
	{
		$this->load->view('layout/mailing/mailing_01');
	}

	public function mailing02()
	{
		$this->load->view('layout/mailing/mailing_02');
	}

	public function mailing03()
	{
		$this->load->view('layout/mailing/mailing_03');
	}
	
	
}
