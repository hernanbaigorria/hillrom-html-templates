<div class="container">
	<div class="row">
		<div class="col-12 text-center" style="padding:30px 0;">
			<a href="<?php echo base_url() ?>home/template_01/" class="btn btn-primary" style="text-transform: uppercase;font-family: Arial;font-weight: 400;">
							TEMPLATE 01
			</a>
			<a href="<?php echo base_url() ?>home/template_02/" class="btn btn-primary" style="text-transform: uppercase;font-family: Arial;font-weight: 400;">
				TEMPLATE 02
			</a>
			<a href="<?php echo base_url() ?>home/template_03/" class="btn btn-primary" style="text-transform: uppercase;font-family: Arial;font-weight: 400;">
				TEMPLATE 03
			</a>
		</div>
	</div>
	<form action="<?=base_url()?>home/generate_code/" method="POST">
		<textarea name="myTextarea" id="myTextarea">
		                                  	                            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		                                  <html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
		                                      <head>
		                                          <!--[if gte mso 9]>
		                                          <xml>
		                                              <o:OfficeDocumentSettings>
		                                                  <o:AllowPNG/>
		                                                  <o:PixelsPerInch>96</o:PixelsPerInch>
		                                              </o:OfficeDocumentSettings>
		                                          </xml>
		                                          <![endif]-->
		                                          <title>Hillrom</title>
		                                          <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		                                          <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
		                                          <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,700;1,400&family=Bitter:ital,wght@0,400;0,700;1,400&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
		                                          <style type="text/css">
		                                              table { 
		                                              font-size:0em;
		                                              border-collapse: collapse;
		                                              }
		                                              a font { 
		                                              border-collapse: collapse;
		                                              line-height:0;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: local('Barlow Regular'), local('Barlow-Regular'), url(https://fonts.gstatic.com/s/barlow/v4/7cHpv4kjgoGqM7E_A8s52Hs.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: local('Barlow Regular'), local('Barlow-Regular'), url(https://fonts.gstatic.com/s/barlow/v4/7cHpv4kjgoGqM7E_Ass52Hs.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: local('Barlow Regular'), local('Barlow-Regular'), url(https://fonts.gstatic.com/s/barlow/v4/7cHpv4kjgoGqM7E_DMs5.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: local('Barlow Medium'), local('Barlow-Medium'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3_-gs6FospT4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: local('Barlow Medium'), local('Barlow-Medium'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3_-gs6VospT4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: local('Barlow Medium'), local('Barlow-Medium'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3_-gs51os.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: italic;
		                                              font-weight: 400;
		                                              src: local('Barlow Italic'), local('Barlow-Italic'), url(https://fonts.gstatic.com/s/barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8RnA.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: italic;
		                                              font-weight: 400;
		                                              src: local('Barlow Italic'), local('Barlow-Italic'), url(https://fonts.gstatic.com/s/barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8RnA.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: italic;
		                                              font-weight: 400;
		                                              src: local('Barlow Italic'), local('Barlow-Italic'), url(https://fonts.gstatic.com/s/barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: local('Barlow Bold'), local('Barlow-Bold'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3t-4s6FospT4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: local('Barlow Bold'), local('Barlow-Bold'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3t-4s6VospT4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Barlow';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: local('Barlow Bold'), local('Barlow-Bold'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3t-4s51os.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Bitter';
		                                              font-style: italic;
		                                              font-weight: 400;
		                                              src: local('Bitter Italic'), local('Bitter-Italic'), url(https://fonts.gstatic.com/s/bitter/v15/rax-HiqOu8IVPmn7erxlJD1img.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Bitter';
		                                              font-style: italic;
		                                              font-weight: 400;
		                                              src: local('Bitter Italic'), local('Bitter-Italic'), url(https://fonts.gstatic.com/s/bitter/v15/rax-HiqOu8IVPmn7erxrJD0.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Bitter';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: local('Bitter Regular'), local('Bitter-Regular'), url(https://fonts.gstatic.com/s/bitter/v15/rax8HiqOu8IVPmn7cYxpPDk.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Bitter';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: local('Bitter Regular'), local('Bitter-Regular'), url(https://fonts.gstatic.com/s/bitter/v15/rax8HiqOu8IVPmn7f4xp.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Bitter';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: local('Bitter Bold'), local('Bitter-Bold'), url(https://fonts.gstatic.com/s/bitter/v15/rax_HiqOu8IVPmnzxKl8DRhfo0s.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Bitter';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: local('Bitter Bold'), local('Bitter-Bold'), url(https://fonts.gstatic.com/s/bitter/v15/rax_HiqOu8IVPmnzxKl8Axhf.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* cyrillic-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 200;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                                              }
		                                              /* cyrillic */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 200;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 200;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 200;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 200;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* cyrillic-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 300;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                                              }
		                                              /* cyrillic */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 300;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 300;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 300;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 300;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* cyrillic-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                                              }
		                                              /* cyrillic */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 400;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* cyrillic-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                                              }
		                                              /* cyrillic */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 500;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* cyrillic-ext */
		                                              @font-face {
		                                              font-family: 'Oswald-SemiBold';
		                                              font-style: normal;
		                                              font-weight: 600;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                                              }
		                                              /* cyrillic */
		                                              @font-face {
		                                              font-family: 'Oswald-SemiBold';
		                                              font-style: normal;
		                                              font-weight: 600;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Oswald-SemiBold';
		                                              font-style: normal;
		                                              font-weight: 600;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Oswald-SemiBold';
		                                              font-style: normal;
		                                              font-weight: 600;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Oswald-SemiBold';
		                                              font-style: normal;
		                                              font-weight: 600;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* cyrillic-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                                              }
		                                              /* cyrillic */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                                              }
		                                              /* vietnamese */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                                              }
		                                              /* latin-ext */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                                              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                                              }
		                                              /* latin */
		                                              @font-face {
		                                              font-family: 'Oswald';
		                                              font-style: normal;
		                                              font-weight: 700;
		                                              src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                                              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                                              }
		                                              /* Client Rendering CSS  */
		                                              a[x-apple-data-detectors] {
		                                              color: inherit!important;
		                                              text-decoration: none!important;
		                                              font-size: inherit!important;
		                                              font-family: inherit!important;
		                                              font-weight: inherit!important;
		                                              line-height: inherit!important;
		                                              }
		                                              #MessageViewBody a {
		                                              color: inherit;
		                                              text-decoration: none;
		                                              font-size: inherit;
		                                              font-family: 'Oswald', sans-serif !important;
		                                              font-weight: inherit;
		                                              line-height: inherit;
		                                              }
		                                              a {
		                                              text-decoration: none;
		                                              }
		                                              * {
		                                              -webkit-text-size-adjust: none;
		                                              }
		                                              body {
		                                              margin: 0 auto !important;
		                                              padding: 0px!important;
		                                              width: 100%;
		                                              min-width: 1px!important;
		                                              margin-right: auto;
		                                              margin-left: auto;
		                                              }
		                                              html, body {
		                                              margin: 0px;
		                                              padding: 0px!important;
		                                              }
		                                              table, td, th {
		                                              border-collapse: collapse;
		                                              border-spacing: 0px;
		                                              mso-table-lspace: 0pt;
		                                              mso-table-rspace: 0pt;
		                                              }
		                                              div, p, a, li, td {
		                                              -webkit-text-size-adjust: none;
		                                              }
		                                              * {
		                                              -webkit-text-size-adjust: none;
		                                              }
		                                              .responsive-hidden {display:block;}
		                                              .responsive-visible {display:none;height:0;width:0;overflow:hidden;}
		                                              img {
		                                              display: block!important;
		                                              }
		                                              .ReadMsgBody {
		                                              width: 100%;
		                                              }
		                                              .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
		                                              line-height: 100%;
		                                              margin: 0px;
		                                              padding: 0px;
		                                              }
		                                              .ExternalClass {
		                                              width: 100%;
		                                              }
		                                              span.MsoHyperlink {
		                                              mso-style-priority:99;
		                                              color:inherit;
		                                              }
		                                              span.MsoHyperlinkFollowed {
		                                              mso-style-priority:99;
		                                              color:inherit;
		                                              }
		                                              .nav .yshortcuts {
		                                              color: #666666
		                                              }
		                                              .blacklink .yshortcuts {
		                                              color: #000000
		                                              }
		                                              .graylink .yshortcuts {
		                                              color: #999999
		                                              }
		                                              .footerLink a {
		                                              color: #999999!important;
		                                              text-decoration: none!important;
		                                              }
		                                              div, button {
		                                              margin: 0!important;
		                                              padding: 0!important;
		                                              display: block!important;
		                                              }
		                                              @media screen and (max-width: 600px) and (min-width: 480px) {
		                                              .scale {
		                                              width: 100%!important;
		                                              min-width: 1px!important;
		                                              max-width: 600px!important;
		                                              height: auto!important;
		                                              max-height: none!important;
		                                              }
		                                              /*  3 Column Image / Text / Button Module: Stacking  */
		                                              .adapt--cta {
		                                              width: 100%!important;
		                                              min-width: 134px!important; 
		                                              }
		                                              /* Background Headline Module CSS */
		                                              .bg {
		                                              width: 100% !important;
		                                              max-width: 600px !important;
		                                              height: auto !important;
		                                              max-height: 100px !important;
		                                              background-image: url(https://picsum.photos/600/100) !important;
		                                              background-position: center !important;
		                                              background-size: cover !important;
		                                              background-repeat: no-repeat !important;
		                                              }
		                                              }
		                                              @media screen and (max-width: 480px) {
		                                              .responsive-hidden {display:none !important;overflow:hidden;width:0;height:0;}
		                                              .responsive-visible {display:block;width:auto;height:auto;overflow:auto;}
		                                              }
		                                              @media (max-width: 480px) {
		                                              /* Template CSS  */
		                                              .fon-size-res-arr {font-size:12px !important;}
		                                              .text-center-responsive {text-align:center !important;}
		                                              .scale {
		                                              width: 100%!important;
		                                              min-width: 1px!important;
		                                              max-width: 480px!important;
		                                              height: auto!important;
		                                              max-height: none!important;
		                                              }
		                                              .stack, .stack-up, .stack-down {
		                                              width: 100%!important;
		                                              max-width: 480px!important;
		                                              height: auto!important;
		                                              display: block!important;
		                                              padding: 0px 0px 0px 0px!important;
		                                              }
		                                              .stack {
		                                              float:left!important;
		                                              }
		                                              .stack-up {
		                                              display: table-header-group!important;
		                                              }
		                                              .stack-down {
		                                              display: table-footer-group!important;
		                                              }
		                                              .hide {
		                                              display: none!important;
		                                              width: 0px!important;
		                                              height: 0px!important;
		                                              max-height: 0px!important;
		                                              padding: 0px 0px 0px 0px!important;
		                                              overflow: hidden!important;
		                                              font-size: 0px!important;
		                                              line-height: 0px!important;
		                                              }
		                                              .show-img {
		                                              display: block!important;
		                                              height: auto!important;
		                                              max-height: inherit!important;
		                                              max-width: 480px!important;
		                                              width: 100%!important;
		                                              line-height: 100%!important;
		                                              padding: 0px!important;
		                                              overflow: visible!important;
		                                              font-size: 14px!important;
		                                              mso-hide: none!important;
		                                              }
		                                              .no--p {
		                                              padding: 0px 0px 0px 0px!important;
		                                              }
		                                              .no--b {
		                                              border: none!important;
		                                              }
		                                              .m-br {
		                                              display: block!important;
		                                              }
		                                              .m-scale {
		                                              width: 100%!important;
		                                              min-width: 1px!important;
		                                              }
		                                              /* Header: Column Navigation CSS */
		                                              .h-logo--p {
		                                              padding: 6px 0px 6px 0px!important;
		                                              }
		                                              /*  2 Column Text / Image Module: Scaling CSS */
		                                              .scale--cta {
		                                              width: 100%!important;
		                                              min-width: 100%!important;
		                                              }
		                                              /*  3 Column Image / Text / Button Module: Stacking CSS */
		                                              .adapt--cta {
		                                              width: 200px!important;
		                                              min-width: 200px!important;
		                                              }
		                                              /*  2 Column Full Width Tile Module: Stacking CSS */
		                                              .c2-tile--p {
		                                              padding: 40px 20px 40px 20px!important;
		                                              }
		                                              .tile-unstack-40 {
		                                              width: 40%!important;
		                                              display: inline-block!important;
		                                              }
		                                              .tile-unstack-60 {
		                                              width: 60%!important;
		                                              display: inline-block!important;
		                                              }
		                                              /* WaCE Footer Family Module CSS */
		                                              .f-fam--p-out {
		                                              padding: 24px 0px 20px 0px!important;
		                                              }
		                                              .f-fam--p-in {
		                                              padding: 0px 54px 8px 54px!important;
		                                              }
		                                              .f-fam--txt {
		                                              font-size: 14px!important;
		                                              padding: 32px 20px 0px 20px!important;
		                                              }
		                                              /* Background Headline Module CSS */
		                                              .bg {
		                                              width: 100% !important;
		                                              max-width: 480px !important;
		                                              height: auto !important;
		                                              max-height: 200px !important;
		                                              background-image: url(https://picsum.photos/480/200) !important;
		                                              background-position: center !important;
		                                              background-size: cover !important;
		                                              background-repeat: no-repeat !important;
		                                              }
		                                              }
		                                              /* Footer Nav Module CSS Media Queries 
		                                              @media (max-width: 380px) {
		                                              .f-nav--txt {
		                                              font-size: 16px!important;
		                                              line-height: 16px!important;
		                                              }
		                                              }
		                                              @media (max-width: 310px) {
		                                              .f-nav--txt {
		                                              font-size: 14px!important;
		                                              line-height: 14px!important;
		                                              } */
		                                              }
		                                          </style>
		                                          <!--[if gte mso 10]>
		                                          <style type="text/css" media="all">
		                                              /* Outlook 2013 Height Fix */
		                                              body, table, td, a { font-family: Arial, Helvetica, sans-serif !important;}
		                                          </style>
		                                          <![endif]-->
		                                      </head>
		                                      <body style="margin: 0px; padding: 0px; background-color:#fff; width: 100%;">
		                                          <!-- Email Container -->
		                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;">
		                                          <tr>
		                                              <td width="100%" align="center" valign="top" style="background-color:#fff;">
		                                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                                                      <tr>
		                                                          <td width="100%" align="left">
		                                                              <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
		                                                                  <tr>
		                                                                      <td width="100%" style="padding: 0px;" class="scale">
		                                                                          <table align="left" border="0" cellpadding="0" cellspacing="0">
		                                                                              <tr>
		                                                                                  <td width="100%" align="left" style="text-align:center;">
		                                                                                      <a href="https://www.descubrehillrom.com/br/connecta/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A" title="Hillrom" target="_blank"><img createnew="true"  src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/716860a6-6a50-4e47-8ebb-db5219780fd7.png" style="display:block; width: 100%; max-width: 175px;border:0px;"></a>
		                                                                                  </td>
		                                                                              </tr>
		                                                                          </table>
		                                                                      </td>
		                                                                  </tr>
		                                                              </table>
		                                                          </td>
		                                                      </tr>
		                                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <a href="https://info-438ac.gr8.com/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A">
		                                                              <div style="display:inline-block;line-height:normal;width:100%">
		                                                                  <img createnew="true" src="https://multimedia.getresponse.com/getresponse-GDfDN/photos/f9b04c1e-833b-47b0-8d8c-d88193033d09.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                                              </div>
		                                                          </a>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <a href="https://info-438ac.gr8.com/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A">
		                                                              <div style="display:inline-block;line-height:normal;width:100%">
		                                                                  <img createnew="true" src="https://multimedia.getresponse.com/getresponse-GDfDN/photos/34a59be4-9241-4004-913e-41b29cf412f2.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                                              </div>
		                                                          </a>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#001871;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <div style="display:inline-block;line-height:normal;width:100%">
		                                                            <p style="font-size: 22px;color: #fff;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:0px 0px 50px;text-align: center;font-weight: normal;">
		                                                                  Desafios e soluções hospitalares<br><span style="color:#5369E5">em tempos de COVID-19</span>
		                                                            </p>
		                                                          </div>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <div style="display:inline-block;line-height:normal;width:100%">
		                                                            <p style="font-size: 24px;color: #201547;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:20px 10px 5px;text-align: center;font-weight: bold;">
		                                                                  Vagas limitadas
		                                                            </p>
		                                                          </div>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <a href="https://info-438ac.gr8.com/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A">
		                                                              <div style="display:inline-block;line-height:normal;width:100%">
		                                                                  <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/46dafd9f-010a-479d-a017-1286a374b931.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                                              </div>
		                                                          </a>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <div style="display:inline-block;line-height:normal;width:100%">
		                                                            <p style="font-size: 17px;color: #201547;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:5px 0px 20px;text-align: center;font-weight: bold;">
		                                                                  Clique na data de sua preferência:
		                                                            </p>
		                                                          </div>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center" style="padding-bottom:20px;">
		                                                          <a href="https://info-579c6.gr8.com/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A" style="display:inline-block !important;line-height:normal;">
		                                                              <img src="https://multimedia.getresponse.com/getresponse-GDfDN/photos/57bfd58c-2d4f-41a8-acc7-3608f07c2388.png" class="scale" style="display:inline-block;margin:5px 10px;max-width: 260px !important;">
		                                                          </a>
		                                                          <a href="https://info-579c6.gr8.com/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A" style="display:inline-block !important;line-height:normal;">
		                                                              <img src="https://multimedia.getresponse.com/getresponse-GDfDN/photos/7d2578ce-4417-4cd7-87d1-f49d84732cd0.png" class="scale" style="display:inline-block;margin:5px 10px;max-width: 260px !important;">
		                                                          </a>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <a href="https://www.descubrehillrom.com/br/connecta?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A">
		                                                              <div style="display:inline-block;line-height:normal;width:100%">
		                                                                  <img createnew="true" src="https://multimedia.getresponse.com/getresponse-GDfDN/photos/50142e0a-406b-4829-a348-88e457f4ce36.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                                              </div>
		                                                          </a>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <div style="display:inline-block;line-height:normal;width:100%">
		                                                            <p style="font-size: 16px;color: #201547;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:30px 0px 30px;text-align: left;font-weight: normal;">
		                                                                  Os desafios que experimentamos com o COVID19 exigem respostas adequadas e rápidas. Portanto, apresentaremos uma solução que ajudará a identificar precocemente a deterioração do paciente e acima de tudo, fornecerá avaliações em tempo real para fazer mais por aqueles que tanto precisam de nós.
		                                                            </p>
		                                                          </div>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <a href="https://www.descubrehillrom.com/br/connecta?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A">
		                                                              <div style="display:inline-block;line-height:normal;width:100%">
		                                                                  <img createnew="true" src="https://multimedia.getresponse.com/getresponse-GDfDN/photos/ee5921b6-f7ef-470b-8d40-64b968d330c6.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                                              </div>
		                                                          </a>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                                      <tr>
		                                          <td width="100%" align="center">
		                                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                                                  <tr>
		                                                      <td width="100%" align="center">
		                                                          <div style="display:inline-block;line-height:normal;width:100%">
		                                                            <p style="font-size: 16px;color: #201547;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:30px 0px 50px;text-align: left;font-weight: normal;">
		                                                                  Nesta sessão, você conhecerá como integrar as ferramentas da plataforma de software Connecta e nossas soluções de sinais vitais para receber alertas antecipados e ativar automaticamente as equipes de resposta rápida.<br><br>  <strong>Agora é sua vez de nos contar sua história, para que nós da Hillrom possamos ajudá-lo a tornar o trabalho mais seguro para o paciente em tempos de Coronavírus e pós pandemia.</strong>
		                                                            </p>
		                                                          </div>
		                                                      </td>
		                                                  </tr>
		                                              </table>
		                                          </td>
		                                      </tr>
		                                  </table>
		                                             <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                                              <tr>
		                                                  <td width="100%" align="left">
		                                                      <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
		                                                          <tr>
		                                                              <td width="100%" style="padding: 0px;" class="scale">
		                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0">
		                                                                      <tr>
		                                                                          <td width="100%" align="left" style="text-align:center;">
		                                                                              <a href="https://www.descubrehillrom.com/br/connecta/?utm_medium=email&utm_source=connecta-webinar-w4-email-addventures&utm_campaign=&utm_term=1A" title="Hillrom" target="_blank"><img createnew="true"  src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/716860a6-6a50-4e47-8ebb-db5219780fd7.png" style="display:block; width: 100%; max-width: 190px;border:0px;"></a>
		                                                                          </td>
		                                                                      </tr>
		                                                                  </table>
		                                                              </td>
		                                                          </tr>
		                                                      </table>
		                                                  </td>
		                                              </tr>
		                                          </table>
		                                          <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#ffffff;" class="scale">
		                                              <tr>
		                                                  <td width="100%" align="center">
		                                                      <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
		                                                          <tr>
		                                                              <td width="100%" align="center" style="padding:20px 0px; color:#666666; text-align:left; font-family:Barlow, sans-serif;font-weight:500; font-size:12px;" class="--txt">
		                                                                  <br>    
		                                                              </td>
		                                                          </tr>
		                                                      <tr></tr>
		                                                      </table>
		                                                  </td>
		                                              </tr>
		                                          </table>
		                                      </body>
		                                  </html>	                            
                          
		</textarea>
		<div class="col-12 text-right" style="margin-top:15px;">
			<input type="submit" value="Descargar HTML" class="btn btn-primary">
		</div>
	</form>
</div>


<SCRIPT language="JavaScript">

var password;

var pass1="hillrom-template";

password=prompt('Por favor ingrese su contraseña',' ');

if (password==pass1)
  alert('Contraseña correcta, precione aceptar para continuar');
else
   {
    window.location="<?=base_url()?>/home/template_02/";
    }

</SCRIPT>