<div class="container">
	<div class="row">
		<div class="col-12 text-center" style="padding:30px 0;">
			<a href="<?php echo base_url() ?>home/template_01/" class="btn btn-primary" style="text-transform: uppercase;font-family: Arial;font-weight: 400;">
							TEMPLATE 01
			</a>
			<a href="<?php echo base_url() ?>home/template_02/" class="btn btn-primary" style="text-transform: uppercase;font-family: Arial;font-weight: 400;">
				TEMPLATE 02
			</a>
			<a href="<?php echo base_url() ?>home/template_03/" class="btn btn-primary" style="text-transform: uppercase;font-family: Arial;font-weight: 400;">
				TEMPLATE 03
			</a>
		</div>
	</div>
	<form action="<?=base_url()?>home/generate_code/" method="POST">
		<textarea name="myTextarea" id="myTextarea">
		                                  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		    <html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
		        <head>
		            <!--[if gte mso 9]>
		            <xml>
		                <o:OfficeDocumentSettings>
		                    <o:AllowPNG/>
		                    <o:PixelsPerInch>96</o:PixelsPerInch>
		                </o:OfficeDocumentSettings>
		            </xml>
		            <![endif]-->
		            <title>Hillrom</title>
		            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		            <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
		            <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,700;1,400&family=Bitter:ital,wght@0,400;0,700;1,400&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
		            <style type="text/css">
		                table { 
		                font-size:0em;
		                border-collapse: collapse;
		                }
		                a font { 
		                border-collapse: collapse;
		                line-height:0;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 400;
		                src: local('Barlow Regular'), local('Barlow-Regular'), url(https://fonts.gstatic.com/s/barlow/v4/7cHpv4kjgoGqM7E_A8s52Hs.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 400;
		                src: local('Barlow Regular'), local('Barlow-Regular'), url(https://fonts.gstatic.com/s/barlow/v4/7cHpv4kjgoGqM7E_Ass52Hs.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 400;
		                src: local('Barlow Regular'), local('Barlow-Regular'), url(https://fonts.gstatic.com/s/barlow/v4/7cHpv4kjgoGqM7E_DMs5.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 500;
		                src: local('Barlow Medium'), local('Barlow-Medium'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3_-gs6FospT4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 500;
		                src: local('Barlow Medium'), local('Barlow-Medium'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3_-gs6VospT4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 500;
		                src: local('Barlow Medium'), local('Barlow-Medium'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3_-gs51os.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: italic;
		                font-weight: 400;
		                src: local('Barlow Italic'), local('Barlow-Italic'), url(https://fonts.gstatic.com/s/barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8RnA.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: italic;
		                font-weight: 400;
		                src: local('Barlow Italic'), local('Barlow-Italic'), url(https://fonts.gstatic.com/s/barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8RnA.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: italic;
		                font-weight: 400;
		                src: local('Barlow Italic'), local('Barlow-Italic'), url(https://fonts.gstatic.com/s/barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 700;
		                src: local('Barlow Bold'), local('Barlow-Bold'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3t-4s6FospT4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 700;
		                src: local('Barlow Bold'), local('Barlow-Bold'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3t-4s6VospT4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Barlow';
		                font-style: normal;
		                font-weight: 700;
		                src: local('Barlow Bold'), local('Barlow-Bold'), url(https://fonts.gstatic.com/s/barlow/v4/7cHqv4kjgoGqM7E3t-4s51os.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Bitter';
		                font-style: italic;
		                font-weight: 400;
		                src: local('Bitter Italic'), local('Bitter-Italic'), url(https://fonts.gstatic.com/s/bitter/v15/rax-HiqOu8IVPmn7erxlJD1img.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Bitter';
		                font-style: italic;
		                font-weight: 400;
		                src: local('Bitter Italic'), local('Bitter-Italic'), url(https://fonts.gstatic.com/s/bitter/v15/rax-HiqOu8IVPmn7erxrJD0.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Bitter';
		                font-style: normal;
		                font-weight: 400;
		                src: local('Bitter Regular'), local('Bitter-Regular'), url(https://fonts.gstatic.com/s/bitter/v15/rax8HiqOu8IVPmn7cYxpPDk.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Bitter';
		                font-style: normal;
		                font-weight: 400;
		                src: local('Bitter Regular'), local('Bitter-Regular'), url(https://fonts.gstatic.com/s/bitter/v15/rax8HiqOu8IVPmn7f4xp.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Bitter';
		                font-style: normal;
		                font-weight: 700;
		                src: local('Bitter Bold'), local('Bitter-Bold'), url(https://fonts.gstatic.com/s/bitter/v15/rax_HiqOu8IVPmnzxKl8DRhfo0s.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Bitter';
		                font-style: normal;
		                font-weight: 700;
		                src: local('Bitter Bold'), local('Bitter-Bold'), url(https://fonts.gstatic.com/s/bitter/v15/rax_HiqOu8IVPmnzxKl8Axhf.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* cyrillic-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 200;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                }
		                /* cyrillic */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 200;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 200;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 200;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 200;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* cyrillic-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 300;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                }
		                /* cyrillic */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 300;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 300;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 300;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 300;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* cyrillic-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 400;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                }
		                /* cyrillic */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 400;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 400;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 400;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 400;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* cyrillic-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 500;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                }
		                /* cyrillic */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 500;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 500;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 500;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 500;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* cyrillic-ext */
		                @font-face {
		                font-family: 'Oswald-SemiBold';
		                font-style: normal;
		                font-weight: 600;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                }
		                /* cyrillic */
		                @font-face {
		                font-family: 'Oswald-SemiBold';
		                font-style: normal;
		                font-weight: 600;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Oswald-SemiBold';
		                font-style: normal;
		                font-weight: 600;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Oswald-SemiBold';
		                font-style: normal;
		                font-weight: 600;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Oswald-SemiBold';
		                font-style: normal;
		                font-weight: 600;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* cyrillic-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 700;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752FD8Ghe4.woff2) format('woff2');
		                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
		                }
		                /* cyrillic */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 700;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752HT8Ghe4.woff2) format('woff2');
		                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		                }
		                /* vietnamese */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 700;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fj8Ghe4.woff2) format('woff2');
		                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
		                }
		                /* latin-ext */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 700;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752Fz8Ghe4.woff2) format('woff2');
		                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
		                }
		                /* latin */
		                @font-face {
		                font-family: 'Oswald';
		                font-style: normal;
		                font-weight: 700;
		                src: url(https://fonts.gstatic.com/s/oswald/v31/TK3iWkUHHAIjg752GT8G.woff2) format('woff2');
		                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
		                }
		                /* Client Rendering CSS  */
		                a[x-apple-data-detectors] {
		                color: inherit!important;
		                text-decoration: none!important;
		                font-size: inherit!important;
		                font-family: inherit!important;
		                font-weight: inherit!important;
		                line-height: inherit!important;
		                }
		                #MessageViewBody a {
		                color: inherit;
		                text-decoration: none;
		                font-size: inherit;
		                font-family: 'Oswald', sans-serif !important;
		                font-weight: inherit;
		                line-height: inherit;
		                }
		                a {
		                text-decoration: none;
		                }
		                * {
		                -webkit-text-size-adjust: none;
		                }
		                body {
		                margin: 0 auto !important;
		                padding: 0px!important;
		                width: 100%;
		                min-width: 1px!important;
		                margin-right: auto;
		                margin-left: auto;
		                }
		                html, body {
		                margin: 0px;
		                padding: 0px!important;
		                }
		                table, td, th {
		                border-collapse: collapse;
		                border-spacing: 0px;
		                mso-table-lspace: 0pt;
		                mso-table-rspace: 0pt;
		                }
		                div, p, a, li, td {
		                -webkit-text-size-adjust: none;
		                }
		                * {
		                -webkit-text-size-adjust: none;
		                }
		                .responsive-hidden {display:block;}
		                .responsive-visible {display:none;height:0;width:0;overflow:hidden;}
		                img {
		                display: block!important;
		                }
		                .ReadMsgBody {
		                width: 100%;
		                }
		                .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
		                line-height: 100%;
		                margin: 0px;
		                padding: 0px;
		                }
		                .ExternalClass {
		                width: 100%;
		                }
		                span.MsoHyperlink {
		                mso-style-priority:99;
		                color:inherit;
		                }
		                span.MsoHyperlinkFollowed {
		                mso-style-priority:99;
		                color:inherit;
		                }
		                .nav .yshortcuts {
		                color: #666666
		                }
		                .blacklink .yshortcuts {
		                color: #000000
		                }
		                .graylink .yshortcuts {
		                color: #999999
		                }
		                .footerLink a {
		                color: #999999!important;
		                text-decoration: none!important;
		                }
		                div, button {
		                margin: 0!important;
		                padding: 0!important;
		                display: block!important;
		                }
		                @media screen and (max-width: 600px) and (min-width: 480px) {
		                .scale {
		                width: 100%!important;
		                min-width: 1px!important;
		                max-width: 600px!important;
		                height: auto!important;
		                max-height: none!important;
		                }
		                /*  3 Column Image / Text / Button Module: Stacking  */
		                .adapt--cta {
		                width: 100%!important;
		                min-width: 134px!important; 
		                }
		                /* Background Headline Module CSS */
		                .bg {
		                width: 100% !important;
		                max-width: 600px !important;
		                height: auto !important;
		                max-height: 100px !important;
		                background-image: url(https://picsum.photos/600/100) !important;
		                background-position: center !important;
		                background-size: cover !important;
		                background-repeat: no-repeat !important;
		                }
		                }
		                .visible-xs {display:none !important;}
		                
		                @media screen and (max-width: 480px) {
		                .responsive-hidden {display:none !important;overflow:hidden;width:0;height:0;}
		                .responsive-visible {display:block;width:auto;height:auto;overflow:auto;}
		                }
		                @media (max-width: 480px) {
		                /* Template CSS  */
		                .visible-xs {display:block !important;}
		                .hidden-xs {display:none !important;}
		                .fon-size-res-arr {font-size:12px !important;}
		                .text-center-responsive {text-align:center !important;}
		                .scale {
		                width: 100%!important;
		                min-width: 1px!important;
		                max-width: 480px!important;
		                height: auto!important;
		                max-height: none!important;
		                }
		                .stack, .stack-up, .stack-down {
		                width: 100%!important;
		                max-width: 480px!important;
		                height: auto!important;
		                display: block!important;
		                padding: 0px 0px 0px 0px!important;
		                }
		                .stack {
		                float:left!important;
		                }
		                .stack-up {
		                display: table-header-group!important;
		                }
		                .stack-down {
		                display: table-footer-group!important;
		                }
		                .hide {
		                display: none!important;
		                width: 0px!important;
		                height: 0px!important;
		                max-height: 0px!important;
		                padding: 0px 0px 0px 0px!important;
		                overflow: hidden!important;
		                font-size: 0px!important;
		                line-height: 0px!important;
		                }
		                .show-img {
		                display: block!important;
		                height: auto!important;
		                max-height: inherit!important;
		                max-width: 480px!important;
		                width: 100%!important;
		                line-height: 100%!important;
		                padding: 0px!important;
		                overflow: visible!important;
		                font-size: 14px!important;
		                mso-hide: none!important;
		                }
		                .no--p {
		                padding: 0px 0px 0px 0px!important;
		                }
		                .no--b {
		                border: none!important;
		                }
		                .m-br {
		                display: block!important;
		                }
		                .m-scale {
		                width: 100%!important;
		                min-width: 1px!important;
		                }
		                /* Header: Column Navigation CSS */
		                .h-logo--p {
		                padding: 6px 0px 6px 0px!important;
		                }
		                /*  2 Column Text / Image Module: Scaling CSS */
		                .scale--cta {
		                width: 100%!important;
		                min-width: 100%!important;
		                }
		                /*  3 Column Image / Text / Button Module: Stacking CSS */
		                .adapt--cta {
		                width: 200px!important;
		                min-width: 200px!important;
		                }
		                /*  2 Column Full Width Tile Module: Stacking CSS */
		                .c2-tile--p {
		                padding: 40px 20px 40px 20px!important;
		                }
		                .tile-unstack-40 {
		                width: 40%!important;
		                display: inline-block!important;
		                }
		                .tile-unstack-60 {
		                width: 60%!important;
		                display: inline-block!important;
		                }
		                /* WaCE Footer Family Module CSS */
		                .f-fam--p-out {
		                padding: 24px 0px 20px 0px!important;
		                }
		                .f-fam--p-in {
		                padding: 0px 54px 8px 54px!important;
		                }
		                .f-fam--txt {
		                font-size: 14px!important;
		                padding: 32px 20px 0px 20px!important;
		                }
		                /* Background Headline Module CSS */
		                .bg {
		                width: 100% !important;
		                max-width: 480px !important;
		                height: auto !important;
		                max-height: 200px !important;
		                background-image: url(https://picsum.photos/480/200) !important;
		                background-position: center !important;
		                background-size: cover !important;
		                background-repeat: no-repeat !important;
		                }
		                }
		                /* Footer Nav Module CSS Media Queries 
		                @media (max-width: 380px) {
		                .f-nav--txt {
		                font-size: 16px!important;
		                line-height: 16px!important;
		                }
		                }
		                @media (max-width: 310px) {
		                .f-nav--txt {
		                font-size: 14px!important;
		                line-height: 14px!important;
		                } */
		                }
		            </style>
		            <!--[if gte mso 10]>
		            <style type="text/css" media="all">
		                /* Outlook 2013 Height Fix */
		                body, table, td, a { font-family: Arial, Helvetica, sans-serif !important;}
		            </style>
		            <![endif]-->
		        </head>
		        <body style="margin: 0px; padding: 0px; background-color:#fff; width: 100%;">
		            <!-- Email Container -->
		            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;">
		            <tr>
		                <td width="100%" align="center" valign="top" style="background-color:#fff;">
		                    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                        <tr>
		                            <td width="100%" align="left">
		                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
		                                    <tr>
		                                        <td width="100%" style="padding: 0px;" class="scale">
		                                            <table align="left" border="0" cellpadding="0" cellspacing="0">
		                                                <tr>
		                                                    <td width="300" align="left" style="text-align:center;">
		                                                        <a href="https://www.descubrehillrom.com/br/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A" title="Descubre Hillrom" target="_blank"><img createnew="true"  src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/716860a6-6a50-4e47-8ebb-db5219780fd7.png" style="display:block; width: 100%; max-width: 190px;border:0px;"></a>
		                                                    </td>
		                                                    <td width="300" align="left" style="text-align:right;">
		                                                        <a href="https://www.descubrehillrom.com/br/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A" title="Descubre Hillrom" target="_blank">
		                                                        <img createnew="true"  src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/3df9ce09-05fd-4372-8b5b-abb3eec368f6.png" style="display:block;float:right;width: 100%; max-width: 140px;border:0px;">
		                                                        </a>
		                                                    </td>
		                                                </tr>
		                                            </table>
		                                        </td>
		                                    </tr>
		                                </table>
		                            </td>
		                        </tr>
		                    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                    <tr>
		                        <td width="100%" align="center">
		                            <a href="https://www.descubrehillrom.com/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A">
		                                <div style="display:inline-block;line-height:normal;width:100%">
		                                    <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/33ad6b30-ab8c-4a91-9ec6-0bf435318fff.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                </div>
		                            </a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                    <tr>
		                        <td width="100%" align="center">
		                            <a href="https://www.descubrehillrom.com/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A">
		                                <div style="display:inline-block;line-height:normal;width:100%">
		                                    <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/d2054c85-8b83-4c2b-8cfa-51b703a7ed45.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                </div>
		                            </a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                    <tr>
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                              <p style="font-size: 16px;color: #000;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:35px 0px 45px;padding:20px;text-align: left;font-weight: normal;">
		                                    Connecta™ status dashboard provides a <strong>high-level snapshot</strong> of how we are progressing with both quantitative and qualitative information.<br><br>    Our Vision: Be a leader in the connected care category and generate awareness for Connecta™ as a unique and comprehensive solution.   What is Connecta™? Connecta™ is a comprehensive software platform that enables bedside data capturing and Early Warning Score analysis to drive earlier intervention of deteriorating patients, improve management of PU, lower Fall risks and facilitate the implementation of early mobility programs for ICU. Actionable alerts save time and reduce errors, providing both the clinician and patient with peace of mind, avoiding ICU readmissions, reducing costs, and allowing patients to go home sooner.
		                              </p>
		                            </div>
		                        </td>
		                    </tr>
		                    
		                    <tr>
		                        <td align="center" role="presentation" style="padding:0px 0 25px;border:none;border-bottom:0 none #000000;border-left:0 none #000000;border-radius:5px;border-right:0 none #000000;border-top:0 none #000000;cursor:auto;font-style:normal;mso-padding-alt:8px 12px;background:none;word-break:break-word;" valign="middle"><a href="https://www.descubrehillrom.com/br/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A" style="display: inline-block;background: #5369E5;color: #FFFFFF;font-family: Montserrat, sans-serif;font-size: 20px;font-style: normal;font-weight: 600;line-height: 100%;margin: 0;text-decoration: none;text-transform: none;padding: 15px 40px;mso-padding-alt: 0px;border-radius: 10px;" target="_blank">Learn More</a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;margin:10px 0;">
		                    <tr><td style="background:#5369E5;font-size:0px;word-break:break-word;"><!--[if mso 
		     IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="8" style="height:8px;"><![endif]--><div style="height:8px;"> </div><!--[if mso 
		     IE]></td></tr></table><![endif]--></td></tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                              <p style="font-size: 20px;color: #001871;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:10px 0px 20px;text-align: left;font-weight:500;">
		                                    HIGHLIGHTS
		                              </p>
		                            </div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td align="left" class="gr-mltext-onfpgt gr-mltext-bmdwpl" style="background:#001871;font-size:0px;padding:19px;word-break:break-word;">
		                            <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1.6;text-align:left;color:#000000;">
		                              
		                                <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><strong><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif"><span style="font-weight: 700">NEW CONNECTA 3.1 LAUNCHED!<br>Reference Sites -  Evaluations</span></span></span></strong></span></p>
		                                
		                                <ul>
		                                
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Copa Star moving forward with 15-day evaluation</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Vila Nova Star installation completed</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Aguas claras has connected 36% of beds (25) - restrictions due to high demand</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">San Javier integration with TASY halfway validated with IT</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Otamendi and Las Lomas EMR integration validated (Biocom)</span></span></span></p>
		                                    </li>
		                                    
		                                </ul>
		                                
		                                
		                                <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><br></p>
		                                <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><strong><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif"><span style="font-weight: 700">Commercial progress</span></span></span></strong></span></p>
		                                
		                                <ul>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Mexico IMSS tender published</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Colombia first Connecta deployment closing before August</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">Chile tenders published (2)</span></span></span></p>
		                                    </li>
		                                    
		                                </ul>
		                                
		                                <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><br></p>
		                                <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><strong><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif"><span style="font-weight: 700">Partnerships</span></span></span></strong></span></p>
		                                
		                                <ul>
		                                  
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">HCA negotiation to offer joint solution and expedite MV connectivity implementations – Initiated process to become certified partners</span></span></span></p>
		                                    </li>
		                                    
		                                    <li style="font-family: Montserrat, Arial, sans-serif;font-size: 16px;color: #FFFFFF;">
		                                        <p style="font-family:Arial;font-size:14px;margin-top:0px;margin-bottom:0px;font-weight:normal;color:#000000;"><span style="color: #FFFFFF"><span style="font-size: 16px"><span style="font-family: Montserrat, Arial, sans-serif">SAP collaboration expanding from Chile (Bupa) to Colombia (Valle del Lili) and Mexico (Star Medica)</span></span></span></p>
		                                    </li>
		                                    
		                                </ul>
		                            </div>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                              <p style="font-size: 20px;color: #001871;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin: 20px 0px 20px;text-align: left;font-weight:500;">
		                                    CONNECTA IN NUMBERS
		                              </p>
		                            </div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                                <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/821d3853-d7fd-43b4-a3f1-9a3bc688ed07.png" width="600px" style="display:block; width:100%;max-width:600px;height:auto;" border="0"  class="">
		                            </div>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                              <p style="font-size: 20px;color: #001871;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin: 20px 0px 20px;text-align: left;font-weight:500;">
		                                    FINANCIAL RESULTS
		                              </p>
		                            </div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                                <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/6b779e5a-45a4-47ad-8584-9492f03ffbe0.png" width="600px" style="display:block; width:100%;max-width:600px;height:auto;" border="0"  class="">
		                            </div>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                    <tr>
		                        <td width="100%" align="center">
		                            <a href="https://www.descubrehillrom.com/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A">
		                                <div style="display:inline-block;line-height:normal;width:100%">
		                                    <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/e60a58ad-7f4a-4df0-aa54-738f31774b1f.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                </div>
		                            </a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                    <tr>
		                        <td width="100%" align="center">
		                            <a href="https://www.descubrehillrom.com/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A">
		                                <div style="display:inline-block;line-height:normal;width:100%">
		                                    <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/5d9f3511-0566-40fe-a583-edcd1beb1ad7.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                </div>
		                            </a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:100%; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                    <tr>
		                        <td width="100%" align="center">
		                            <a href="https://www.descubrehillrom.com/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A">
		                                <div style="display:inline-block;line-height:normal;width:100%">
		                                    <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/895c7434-868d-47f3-aa81-45c1f3652ff8.png" width="600" style="display:block; max-width:600px; width:100%; height:auto;" border="0"  class="">
		                                </div>
		                            </a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;margin:10px 0;">
		                    <tr><td style="background:#5369E5;font-size:0px;word-break:break-word;"><!--[if mso 
		     IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="8" style="height:8px;"><![endif]--><div style="height:8px;"> </div><!--[if mso 
		     IE]></td></tr></table><![endif]--></td></tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                              <p style="font-size: 20px;color: #001871;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin: 20px 0px 20px;text-align: left;font-weight:500;">
		                                    MAIN OBJECTIVES
		                              </p>
		                            </div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                                <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/0af64e7f-5c25-4422-a74f-e409e32abae9.png" width="600px" style="display:block; width:100%;max-width:600px;height:auto;" border="0"  class="">
		                            </div>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;margin:10px 0;">
		                    <tr><td style="background:#5369E5;font-size:0px;word-break:break-word;"><!--[if mso 
		     IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="8" style="height:8px;"><![endif]--><div style="height:8px;"> </div><!--[if mso 
		     IE]></td></tr></table><![endif]--></td></tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		        <tr>
		            <td width="100%" align="center">
		                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="line-height:0;font-size:0;">
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                              <h3 style="font-size: 16px;color: #001871;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin: 20px 0px 5px;text-align: left;font-weight:500;"> QUESTIONS OR COMMENTS?</h3>
		                              <p style="font-size: 14px;color: #001871;display: block;font-family:Montserrat, sans-serif;line-height: normal;padding: 0 20px;margin:5px 0px 25px;text-align: left;font-weight:300;">
		                                    Feel free to reach out directly to: <a href="mailto:Leni.Vila@hillrom.com?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A">Leni Vila</a>
		                              </p>
		                            </div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td width="100%" align="center">
		                            <div style="display:inline-block;line-height:normal;width:100%">
		                                <img createnew="true" src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/39bd2b73-471c-47ec-b359-a88f52ac6797.jpg" width="auto" style="display:block; width:auto;max-width:600px;height:auto;" border="0"  class="">
		                            </div>
		                        </td>
		                    </tr>
		                    <tr><td style="font-size:0px;padding:15px 0 20px;word-break:break-word;"><p style="border-top:solid 1px #4F4F4F;font-size:1px;margin:0px auto;width:100%;"></p><!--[if mso 
		     IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #4F4F4F;font-size:1px;margin:0px auto;width:590px;" role="presentation" width="590px" ><tr><td style="height:0;line-height:0;">  
		                    </td></tr></table><![endif]--></td></tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#fff;" class="scale">
		                <tr>
		                    <td width="100%" align="left">
		                        <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
		                            <tr>
		                                <td width="100%" style="padding: 0px;" class="scale">
		                                    <table align="left" border="0" cellpadding="0" cellspacing="0">
		                                        <tr>
		                                            <td width="100%" align="left" style="text-align:center;">
		                                                <a href="https://www.descubrehillrom.com/br/connecta?utm_medium=email&utm_source=21-0447-dh-b-connecta-weekly-dashboard-06-07-email-addventures&utm_campaign=connecta-weekly-dashboard-06-07&utm_term=1A" title="Hillrom" target="_blank"><img createnew="true"  src="https://us-ms.gr-cdn.com/getresponse-GDfDN/photos/716860a6-6a50-4e47-8ebb-db5219780fd7.png" style="display:block; width: 100%; max-width: 190px;border:0px;"></a>
		                                            </td>
		                                        </tr>
		                                    </table>
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		            </table>
		            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:600px; width:600px; background-color:#ffffff;" class="scale">
		                <tr>
		                    <td width="100%" align="center">
		                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
		                            <tr>
		                                <td width="100%" align="center" style="padding:20px 0px; color:#666666; text-align:left; font-family:Barlow, sans-serif;font-weight:500; font-size:12px;" class="--txt">
		                                    <br>           
		                                </td>
		                            </tr>
		                        <tr></tr>
		                        </table>
		                    </td>
		                </tr>
		            </table>
		        </body>
		    </html>                             
		</textarea>
		<div class="col-12 text-right" style="margin-top:15px;">
			<input type="submit" value="Descargar HTML" class="btn btn-primary">
		</div>
	</form>
</div>

<SCRIPT language="JavaScript">

var password;

var pass1="hillrom-template";

password=prompt('Por favor ingrese su contraseña',' ');

if (password==pass1)
  alert('Contraseña correcta, precione aceptar para continuar');
else
   {
    window.location="<?=base_url()?>/home/template_03/";
    }

</SCRIPT>