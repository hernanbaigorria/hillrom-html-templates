<!DOCTYPE html>
<html>
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="<?= $description ?>">
	  <meta name="keywords" content="<?= $keywords ?>">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
		  <meta property="og:title" content="<?= $title ?>" />
		  <meta property="og:type" content="<?= $ogType ?>" />
		  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
		  <meta property="og:image" content="<?= base_url().$image ?>" />
		  <meta property="og:description" content="<?= $description ?>" />
		  
	  <meta name="facebook-domain-verification" content="68lp4l4f1a7p2kojoipmcwi6pt12mc" />

	  <link rel='stylesheet' href='https://npmcdn.com/flickity@2/dist/flickity.css'>
	  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>asset/img/favicon.png?">  
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	  
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/main.css?v=<?php echo time() ?>" type="text/css" media="all" />
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/animations.css" media="all" />
	  <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,800,900" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/font-awesome.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/paginador/css/animate.css">
	  
	  <style type="text/css">
	  	.codeBlocks {display:none;}
	  </style>
	  
	  <script>var base_url = "<?php echo base_url() ?>";</script>
	  <?= $_styles ?>
   </head>
   <body>

	   <?= $header ?>        
	   <?= $content ?>
   	<?= $footer ?>


   	<script src='<?=base_url()?>asset/tinymce/tinymce.min.js'></script>

   	<script type="text/javascript">
   	  tinymce.init({
   	      selector: 'textarea#myTextarea',
   	      plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
   	      imagetools_cors_hosts: ['picsum.photos'],
   	      menubar: 'file edit view insert format tools table help',
   	      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
   	      toolbar_sticky: true,
   	      autosave_ask_before_unload: true,
   	      autosave_interval: "30s",
   	      autosave_prefix: "{path}{query}-{id}-",
   	      autosave_restore_when_empty: false,
   	      autosave_retention: "2m",
   	      image_advtab: true,
   	      /*content_css: '//www.tiny.cloud/css/codepen.min.css',*/
   	      link_list: [
   	          { title: 'My page 1', value: 'https://www.codexworld.com' },
   	          { title: 'My page 2', value: 'https://www.xwebtools.com' }
   	      ],
   	      image_list: [
   	          { title: 'My page 1', value: 'https://www.codexworld.com' },
   	          { title: 'My page 2', value: 'https://www.xwebtools.com' }
   	      ],
   	      image_class_list: [
   	          { title: 'None', value: '' },
   	          { title: 'Some class', value: 'class-name' }
   	      ],
   	      importcss_append: true,
   	      file_picker_callback: function (callback, value, meta) {
   	          /* Provide file and text for the link dialog */
   	          if (meta.filetype === 'file') {
   	              callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
   	          }
   	      
   	          /* Provide image and alt text for the image dialog */
   	          if (meta.filetype === 'image') {
   	              callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
   	          }
   	      
   	          /* Provide alternative source and posted for the media dialog */
   	          if (meta.filetype === 'media') {
   	              callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
   	          }
   	      },
   	      templates: [
   	          { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
   	          { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
   	          { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
   	      ],
   	      template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
   	      template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
   	      height: 600,
   	      image_caption: true,
   	      quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
   	      noneditable_noneditable_class: "mceNonEditable",
   	      toolbar_mode: 'sliding',
   	      contextmenu: "link image imagetools table",
   	  });
   	</script>

	  <script type="text/javascript">
	  	$(".form-contacto").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.loading').show('slow');
	  		    },
	  		    success: function(response){
	  		    	if(response == 'empresa'){
	  		         	window.location.replace("<?php echo base_url()?>home/gracias_empresa/");
	  		    	} else {
	  		         	window.location.replace("<?php echo base_url()?>home/gracias_productor/");
	  		         }
	  		    }
	  		});
	  	});
	  	
	  </script>
	  
   </body>
</html>